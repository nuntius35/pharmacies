from bs4 import BeautifulSoup
import glob
import os
from timeit import default_timer as timer


def html_to_text(id, filepath):
    with open(filepath, 'r') as html_file:
        try:
            soup = BeautifulSoup(html_file, features='lxml')
        except UnicodeDecodeError:
            print('[html_to_text] [' + id + '] Cannot read ' + filepath, flush=True)
            return('')

    # remove all script and style elements
    for script in soup(["script", "style"]):
        script.extract()

    text = soup.get_text()
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text = '\n'.join(chunk for chunk in chunks if chunk)

    return(text)


def page_to_text(id, dirpath):
    outfile = dirpath + '/page.txt'
    if (not os.path.isfile(outfile)):
        print('[page_to_text] [' + id + '] Processing data.......', flush=True)
        text = ''
        for filepath in glob.iglob(dirpath + '/*.html'):
            text += html_to_text(id, filepath)
        text = set(text.split('\n'))
        with open(outfile, 'w') as f:
            for line in text:
                f.write(line+'\n')
        print('[page_to_text] [' + id + '] Finished writing "page.txt".', flush=True)
    else:
        print('[place_to_txt] [' + id + '] "page.txt" already exists, skipping.', flush=True)
    return


def place_to_txt(id, url):
    dirpath = 'webdata/' + id
    createdir = 'mkdir ' + dirpath + ' 2> /dev/null'
    chdir = 'cd ' + dirpath
    wgetcmd = 'wget --recursive \
                    --level=3 \
                    --adjust-extension \
                    --no-directories \
                    --quota=6m \
                    --timeout=60 \
                    --wait=1 \
                    --random-wait \
                    --user-agent="Mozilla/5.0" \
                    --reject "*.js,*.css,*.ico,*.txt,*.gif,*.jpg,*.jpeg,\
                              *.png, *.mp3,*.mp4,*.pdf,*.tgz,*.flv,*.avi,\
                              *.mpeg,*.webm,*.iso,*.svg,*.eot,*.ttf,\
                              *.woff,*.tmp" \
                    --ignore-tags=img,link,script \
                    --header="Accept: text/html" ' + url + ' 2> wget.log'
    err = os.system(createdir)
    if not err:
        print('[place_to_txt] [' + id + '] Try to crawl ' + url, flush=True)
        start = timer()
        os.system(chdir + ' && ' + wgetcmd)
        end = timer()
        print('[place_to_txt] [' + id + '] Crawling finished in......[{:7.3f} s]'.format(end-start), flush=True)
    else:
        print('[place_to_txt] [' + id + '] Directory already exists, no crawling.', flush=True)
    page_to_text(id, dirpath)
    return
