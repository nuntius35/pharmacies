# README

This little projects analyzes websites of pharmacies.
The aim is to get an overview if and how often pharmacies mention alternative medicine on their website.

## Run the analysis

* Type `make`

## View the results

* [Interactive map](https://nuntius35.gitlab.io/pharmacies/)

---

[Homepage of Andreas Geyer-Schulz](https://nuntius35.gitlab.io)
