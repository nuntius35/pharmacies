import json
import os
from collections import Counter
from timeit import default_timer as timer

eso = [
   ['Homöopathie',
    'homöopathisch',
    'homöopathischer',
    'homöopathischen',
    'Urtinktur',
    'Potenzierung',
    'Potenzen',
    'Potenzstufen',
    'verschüttelt',
    'similibus',
    'Hahnemann',
    'Hahnemanns',
    'D4',
    'D6'],
   ['Schüssler',
    'Schüßler',
    'Schüsslersalz',
    'Schüsslersalze',
    'Salze',
    'Antlitzanalyse',
    'Antlitz',
    'Antlitzdiagnose'],
   ['Bach-Blüten',
    'Bachblüten',
    'Bachblütentherapie',
    'Bach-Blütentherapie'],
   ['Aroma',
    'Aromatherapie'],
   ['chinesischen',
    'chinesische'
    'TCM'],
   ['CBD',
    'Cannabidiol',
    'Cannabis',
    'Cannabinoide'],
   ['Spagyrik',
    'Krauß',
    'Pekana',
    'Strathmeyer',
    'Zimpel',
    'Glückselig',
    'Bernus',
    'Ayurveda',
    'komplementär',
    'komplementäre',
    'Komplementärmedizin',
    'TEM',
    'Kinesiologie',
    'Goodheart',
    'Haaranalyse',
    'Pulsdiagnose',
    'Feldenkrais',
    'Akupunktur',
    'Akkupunktur',
    'Akupunkt',
    'tibetisch',
    'tibetische',
    'Reiki',
    'Energiefeld',
    'Energieheilsystem',
    'Ketabi']
]

esowords = [word for category in eso for word in category]


def analyze(file='osmdata/pharmacies.json'):
    with open(file, 'r') as data_file:
        data = json.load(data_file)

    print('Analyzing data..............................', end='', flush=True)
    start = timer()
    results = {"type": "FeatureCollection",
               "features": []}

    for pharmacy in data['elements']:
        id = str(pharmacy['id'])

        if pharmacy['type'] in ['node']:
            lon = pharmacy['lon']
            lat = pharmacy['lat']
        elif pharmacy['type'] in ['way']:
            lon = pharmacy['center']['lon']
            lat = pharmacy['center']['lat']
        else:
            lon = 0
            lat = 0

        if 'name' in pharmacy['tags']:
            name = pharmacy['tags']['name']
        else:
            name = 'Ohne Namen'

        if 'website' in pharmacy['tags']:
            url = pharmacy['tags']['website']
        elif 'contact:website' in pharmacy['tags']:
            url = pharmacy['tags']['contact:website']
        else:
            url = ''

        stats = [0] * len(eso)

        pagefile = 'webdata/' + id + '/page.txt'
        if (os.path.isfile(pagefile)):
            with open(pagefile, 'r') as f:
                filtered_words = [word for word in f.read().split()
                                  if word in esowords]
            words = Counter(filtered_words)
            for word, count in words.items():
                for i in range(len(eso)):
                    if word in eso[i]:
                        stats[i] += count

        geojson = {"type": "Feature",
                   "geometry": {"type": "Point",
                                "coordinates": [lon, lat]},
                   "properties": {'stats': stats,
                                  'name': name,
                                  'website': url,
                                  'id': id}
                   }
        results["features"].append(geojson)
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
    print('Writing geojson.............................', end='', flush=True)
    start = timer()
    with open('pharmacies.geojson', 'w') as data_file:
        json.dump(results, data_file, ensure_ascii=False)
        data_file.write('\n')
    end = timer()
    print('[{:7.3f} s]'.format(end-start))
    return


if __name__ == '__main__':
    analyze()
