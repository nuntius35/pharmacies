import json
import os.path


def statistics(file='pharmacies.geojson'):
    if (not os.path.isfile(file)):
        print('[statistics] No data available, quitting.')
        return

    with open(file, 'r') as data_file:
        data = json.load(data_file)

    c = dict(total=0, noweb=0, none=0, some=0, much=0)
    for pharmacy in data['features']:
        c['total'] += 1
        stats = pharmacy['properties']['stats']
        website = pharmacy['properties']['website']
        if website == '':
            c['noweb'] += 1
        else:
            ss = sum(stats)
            if ss == 0:
                c['none'] += 1
            elif ss > 0 and ss <= 7:
                c['some'] += 1
            else:
                c['much'] += 1
    print(c)
    with open('public/res/counts.js', 'w') as f:
        f.write('var counts = ')
        json.dump(c, f)
    return


if __name__ == '__main__':
    statistics()
