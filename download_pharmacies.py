import json
import requests


def download_pharmacies(file='osmdata/pharmacies.json'):
    '''
    Retrieves all pharmacies from OpenStreetMap using Overpass API
    Observe the usage policy of the mains Overpass API instance.
    '''
    overpass_url = 'https://overpass-api.de/api/interpreter'
    overpass_query = '''[timeout:600][out:json];
                        (area[name="Österreich"][admin_level=2][boundary=administrative];
                         area[name="Deutschland"][admin_level=2][boundary=administrative];
                        )->.district;
                        (node(area.district)["amenity"="pharmacy"];
                        way(area.district)["amenity"="pharmacy"];);
                        out center;'''
    response = requests.get(overpass_url,
                            params={'data': overpass_query})
    data = response.json()

    with open(file, 'w') as data_file:
        json.dump(data, data_file, indent=4, ensure_ascii=False)
    return


if __name__ == '__main__':
    download_pharmacies()
