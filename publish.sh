#!/bin/bash
echo "var pharmacies = " | cat - pharmacies.geojson > public/res/pharmacies-geojson.js
sed -i "s/[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}/$(date '+%Y-%m-%d')/g" public/index.html
