import json
import argparse
import os.path
import multiprocessing as mp
from timeit import default_timer as timer
from datetime import datetime
from place_to_txt import place_to_txt
from download_pharmacies import download_pharmacies


def crawl_pharmacies(file='osmdata/pharmacies.json'):
    parser = argparse.ArgumentParser(
        description='Crawl websites of pharmacies.')
    parser.add_argument('-f',
                        '--force',
                        action='store_true',
                        help='Force download of data.')
    args = parser.parse_args()

    if (not os.path.isfile(file)) or args.force:
        print('Retrieving data from Overpass API.........',
              end='', flush=True)
        start = timer()
        download_pharmacies(file)
        end = timer()
        print('[{:7.3f} s]'.format(end-start))
    else:
        mtime = datetime.fromtimestamp(os.path.getmtime(file))
        mtime = mtime.strftime('%Y-%m-%d %H:%M:%S')
        print('Using saved OSM data from {0}'.format(mtime))

    with open(file, 'r') as data_file:
        data = json.load(data_file)

    pool = mp.Pool(processes=64)
    arglist = []
    for pharmacy in data['elements']:
        id = str(pharmacy['id'])
        url = ''
        if 'website' in pharmacy['tags']:
            url = pharmacy['tags']['website']
        elif 'contact:website' in pharmacy['tags']:
            url = pharmacy['tags']['contact:website']
        if url:
            arglist.append((id, url))
    print('[crawl_pharmacies] Start crawling...........\n',
          end='', flush=True)
    start = timer()
    pool.starmap(place_to_txt, arglist)
    end = timer()
    print('[crawl_pharmacies] Finished in..............',
          end='', flush=True)
    print('[{:7.3f} s]'.format(end-start))
    return


if __name__ == '__main__':
    crawl_pharmacies()
