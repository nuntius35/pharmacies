all: page

data:
	python3 download_pharmacies.py --force

crawl: osmdata/pharmacies.json
	python3 crawl_pharmacies.py

page: public/res/pharmacies-geojson.js public/res/counts.js

osmdata/pharmacies.json:
	python3 download_pharmacies.py

pharmacies.geojson: osmdata/pharmacies.json crawl
	python3 analyze.py

public/res/pharmacies-geojson.js: pharmacies.geojson
	./publish.sh

public/res/counts.js: pharmacies.geojson
	python3 statistics.py

cleanall:
	rm -f pharmacies.geojson
	rm -f osmdata/pharmacies.json
	rm -rf webdata/[0-9]*

.PHONY: cleanall
